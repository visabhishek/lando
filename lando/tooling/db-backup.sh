#!/bin/bash
if test $1; then
  echo "Dumping database from $1..."
  cd /app/docroot
  USAGE="Usage: lando db-backup [env] where [env] is dev, test or prod."
  BACKUP=[project]-$1-$(date +%Y%m%d%H%M).sql.gz
  drush @$1 cc all && drush @$1 sql-dump | gzip > $BACKUP
  [[ ! -f $BACKUP || $? != 0 ]] && echo "$USAGE" && exit 1
  SIZE=$(ls -lh $BACKUP | cut -d' ' -f5)
  echo "Dumped database to $(pwd)/$BACKUP ($SIZE)."
else
  echo "$USAGE"
  exit 1
fi
