<?php

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site [project], environment dev.
$aliases['dev'] = array(
  'root' => '/var/www/html/[project].dev/docroot',
  'ac-site' => '[project]',
  'ac-env' => 'dev',
  'ac-realm' => 'devcloud',
  'uri' => '[project-dev].devcloud.acquia-sites.com',
  'remote-host' => '[project-dev].ssh.devcloud.acquia-sites.com',
  'remote-user' => '[project].dev',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  ),
);
$aliases['dev.livedev'] = array(
  'parent' => '@[project].dev',
  'root' => '/mnt/gfs/[project].dev/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site [project], environment prod.
$aliases['prod'] = array(
  'root' => '/var/www/html/[project].prod/docroot',
  'ac-site' => '[project]',
  'ac-env' => 'prod',
  'ac-realm' => 'devcloud',
  'uri' => '[project-prod].devcloud.acquia-sites.com',
  'remote-host' => '[project-prod].ssh.devcloud.acquia-sites.com',
  'remote-user' => '[project].prod',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  ),
);
$aliases['prod.livedev'] = array(
  'parent' => '@[project].prod',
  'root' => '/mnt/gfs/[project].prod/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site [project], environment test.
$aliases['test'] = array(
  'root' => '/var/www/html/[project].test/docroot',
  'ac-site' => '[project]',
  'ac-env' => 'test',
  'ac-realm' => 'devcloud',
  'uri' => '[project-test].devcloud.acquia-sites.com',
  'remote-host' => '[project-test].ssh.devcloud.acquia-sites.com',
  'remote-user' => '[project].test',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  ),
);
$aliases['test.livedev'] = array(
  'parent' => '@[project].test',
  'root' => '/mnt/gfs/[project].test/livedev/docroot',
);
require "local.drushrc.inc";
